import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'equipamentos', loadChildren: './pages/equipamentos/equipamentos.module#EquipamentosPageModule' },
  { path: 'emprestimos', loadChildren: './pages/emprestimos/emprestimos.module#EmprestimosPageModule' },
  { path: 'emprestimos/equipamento/:id', loadChildren: './pages/equipamento-detalhe/equipamento-detalhe.module#EquipamentoDetalhePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
