import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.scss'],
})
export class TopoComponent implements OnInit {

  @Input() 
  titulo: String;

  @Input()
  icone: String;

  constructor() { }

  ngOnInit() {}

}
