import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TopoComponent } from './topo/topo.component';



@NgModule({
  declarations: [TopoComponent],
  imports: [
    IonicModule,
    CommonModule
  ],
  exports: [
    TopoComponent
  ]
})
export class EquipamentosModule { }
