import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import { EquipamentosModule } from 'src/app/shared/components/equipamentos/equipamentos.module'

import { IonicModule } from '@ionic/angular';

import { EquipamentosPage } from './equipamentos.page';

const routes: Routes = [
  {
    path: '',
    component: EquipamentosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EquipamentosModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EquipamentosPage]
})
export class EquipamentosPageModule {}
