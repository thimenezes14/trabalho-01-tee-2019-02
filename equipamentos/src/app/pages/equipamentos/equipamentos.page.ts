import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Equipamento } from './Equipamento';
import { Emprestimo } from '../emprestimos/Emprestimo';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-equipamentos',
  templateUrl: './equipamentos.page.html',
  styleUrls: ['./equipamentos.page.scss'],
})
export class EquipamentosPage {

  equipamento: Equipamento;
  equipamentoForm: FormGroup;

  imagem: string;

  listaEquipamentos: Array<Equipamento>;
  msgErro: boolean = false;

  constructor(private formBuilder: FormBuilder, private nav: NavController, private alert: AlertController) {
    this.equipamentoForm = this.formBuilder.group({
      nomeEquipamento: ['', Validators.required],
      marcaEquipamento: ['', Validators.required],
      categoriaEquipamento: ['', Validators.required],
      quantidadeEquipamento: ['', Validators.required],
      corEquipamento: ['', Validators.required],
      imagemEquipamento: ['', Validators.required]
    });

    this.equipamento = new Equipamento(null,null,null,null,null,null);
    this.listaEquipamentos = this.equipamento.mostrarEquipamentos();
  }

  cadastrarEquipamento() {
    let CadEquipamento = this.equipamentoForm.value;

    this.equipamento = new Equipamento(
      CadEquipamento.nomeEquipamento,
      CadEquipamento.marcaEquipamento,
      CadEquipamento.quantidadeEquipamento,
      CadEquipamento.categoriaEquipamento,
      CadEquipamento.corEquipamento,
      this.imagem
    );

    console.log(this.equipamento);

    if(this.equipamentoForm.valid){
      this.equipamento.salvarEquipamento();
      this.msgErro = false;
      this.equipamentoForm.reset();
      this.listaEquipamentos = this.equipamento.mostrarEquipamentos();
    }
    else
      this.msgErro = true;

  }

  async excluir(id) {
    const form = await this.alert.create({
        header: 'CONFIRMAR EXCLUSÃO',
        message: 'Tem certeza que deseja excluir este equipamento?',
        buttons: [
            {text: 'NÃO'},
            {text: 'SIM', handler: data => this.removerEquipamento(id)}
        ]
    });

    form.present();
  
  }

  async atualizar(item) {
    const form = await this.alert.create({
        header: 'ALTERAR QUANTIDADE',
        message: 'Por favor, altere para a quantidade desejada: ',
        inputs: [{name: 'quantidade', value: item.quantidade, type: 'number', min: '1'}],
        buttons: [
            {text: 'CANCELAR'},
            {text: 'CONFIRMAR', handler: data => this.atualizarEquipamento(item.id, data.quantidade)}
        ]
    });

    form.present();
  }

  atualizarEquipamento(id: Number, quantidade: Number) {
    this.equipamento.atualizarQuantidadeEquipamento(id, quantidade);
    this.listaEquipamentos = this.equipamento.mostrarEquipamentos();
  }

  removerEquipamento(id: Number) {
    let emp = new Emprestimo(null,null,null).detalharEmprestimo(id);
    if(emp.length == 0)
      this.equipamento.removerEquipamento(id);
    this.listaEquipamentos = this.equipamento.mostrarEquipamentos();
  }

  files: any[];

  validarImagemEquipamento(input) :void{
		if (input.target.files && input.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e: any) => {
				this.imagem = e.target.result;
			};
			reader.readAsDataURL(input.target.files[0]);
		}
	}

}
