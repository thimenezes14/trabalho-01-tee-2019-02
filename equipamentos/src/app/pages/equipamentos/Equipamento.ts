export class Equipamento  {
    private id: Number;
    private nome: string;
    private marca: string;
    private quantidade: Number;
    private disponiveis: Number = this.quantidade;
    private categoria: string;
    private cor: string;
    private imagem: string;
    
    private localStorageName: string;

    constructor(nome: string, marca: string, quantidade: Number, categoria: string, cor: string, imagem: string) {
        this.id = new Date().getTime();
        this.nome = nome;
        this.marca = marca;
        this.quantidade = quantidade;
        this.categoria = categoria;
        this.cor = cor;
        this.imagem = imagem;
        this.localStorageName = 'equipamentos';
    }
    
    public getId() {
        return this.id;
    }
    public getNome() {
        return this.nome;
    }
    public getMarca() {
        return this.marca;
    }
    public getQuantidade() {
        return this.quantidade;
    }
    public getDisponiveis() {
        return this.disponiveis;
    }
    public getCategoria() {
        return this.categoria;
    }
    public getCor() {
        return this.cor;
    }
    public getLocalStorageName() {
        return this.localStorageName;
    }
    public getImagem() {
        return this.imagem;
    }

    public salvarEquipamento() {
        const equipamento = {
            id: this.id,
            nome: this.nome,
            marca: this.marca,
            quantidade: this.quantidade,
            disponiveis: this.quantidade,
            categoria: this.categoria,
            cor: this.cor,
            imagem: this.imagem
        }

        let equipamentos = JSON.stringify(this.__agrupar(equipamento));
        localStorage.setItem(this.localStorageName, equipamentos);
    }

    public removerEquipamento(idEquipamento: Number) {
        let equipamentos = this.mostrarEquipamentos();
        let equipamentoRemover = Array();

    
        if(equipamentos !== null && equipamentos.length > 0) {
          equipamentos.forEach(item => {
            equipamentoRemover.push(item);
          });

          let filtrados = equipamentoRemover.filter(equip => equip.id == idEquipamento);
          
          if(equipamentoRemover[0].disponiveis == equipamentoRemover[0].quantidade) {
            filtrados = equipamentoRemover.filter(equip => equip.id != idEquipamento);
            localStorage.setItem(this.localStorageName, JSON.stringify(filtrados));
          }
        }
    }

    public atualizarQuantidadeEquipamento(idEquipamento: Number, quantidade: any) {
        let equipamentos = this.mostrarEquipamentos();
        let equipamentoAtualizar = Array();
    
        if(equipamentos !== null && equipamentos.length > 0) {
          equipamentos.forEach(item => {
            if(item.id == idEquipamento) {
                let emprestados = item.quantidade - item.disponiveis;

                if(quantidade >= 1 && quantidade >= emprestados) {
                    item.disponiveis += (quantidade - item.quantidade);
                    item.quantidade = quantidade;
                }
            }
            equipamentoAtualizar.push(item);
          });
          
          localStorage.setItem(this.localStorageName, JSON.stringify(equipamentoAtualizar));

          return true;
        }

        return false;
    }

    public atualizarQuantidadeDisponiveisEquipamento(idEquipamento: Number, quantidade: any, devolucao: boolean) {
        let equipamentos = this.mostrarEquipamentos();
        let equipamentoAtualizar = Array();
        let errors = 0;
    
        if(equipamentos !== null && equipamentos.length > 0) {
          equipamentos.forEach(item => {
            if(item.id == idEquipamento) {
                if(!devolucao) {
                    if(quantidade >= 1 && quantidade <= item.disponiveis) {
                        item.disponiveis -= parseInt(quantidade);
                    } else {
                        errors++;
                    }
                } else {
                    item.disponiveis += parseInt(quantidade);
                }
            }
            equipamentoAtualizar.push(item);
          });

          if(errors > 0) {
              return false;
          }
          
          localStorage.setItem(this.localStorageName, JSON.stringify(equipamentoAtualizar));

          return true;
        }

        return false;
    }

    public detalharEquipamento(idEquipamento: any) {
        let equipamentos = this.mostrarEquipamentos();
        let equipamentoDetalhar = Array();
    
        if(equipamentos !== null && equipamentos.length > 0) {
          equipamentos.forEach(item => {
            equipamentoDetalhar.push(item);
          });

          let filtrados = equipamentoDetalhar.filter(equip => equip.id == idEquipamento);
          return filtrados[0];
        }
    }

    public mostrarEquipamentos() {
        return (JSON.parse(localStorage.getItem(this.localStorageName)));
    }

    private __agrupar(equipamento: any) {
        let equipamentos = JSON.parse(localStorage.getItem(this.localStorageName));
        let listaEquipamentos = Array();
    
        if(equipamentos !== null && equipamentos.length > 0) {
          equipamentos.forEach(item => {
            listaEquipamentos.push(item);
          });
        }
    
        listaEquipamentos.push(equipamento);
        return listaEquipamentos;
    }
}