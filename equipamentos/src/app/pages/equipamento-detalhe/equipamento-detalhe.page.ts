import { Component, OnInit } from '@angular/core';
import { Equipamento } from '../equipamentos/Equipamento';
import { Emprestimo } from '../emprestimos/Emprestimo';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-equipamento-detalhe',
  templateUrl: './equipamento-detalhe.page.html',
  styleUrls: ['./equipamento-detalhe.page.scss'],
})
export class EquipamentoDetalhePage implements OnInit {

  equipamento;
  id: string;

  equipamentos: Array<Equipamento>;


  constructor(private nav: NavController, route: ActivatedRoute, private alert: AlertController) {
    this.id = route.snapshot.paramMap.get('id');
    this.equipamento = new Equipamento(null,null,null,null,null,null).detalharEquipamento(this.id);

    this.verificarAcesso();
  }
  
  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.verificarAcesso();
  }

  verificarAcesso() {
    if(!this.equipamento) {
      window.location.href = '/';
    }
  }

  async emprestar(item) {
    const form = await this.alert.create({
        header: 'EMPRESTAR EQUIPAMENTO',
        message: 'Por favor, preencha os dados para empréstimo',
        inputs: [
          {placeholder: 'Quantidade', name: 'quantidade', type: 'number', min: '1', max: item.disponiveis},
          {placeholder: 'Solicitante', name: 'solicitante', type: 'text'},

        ],
        buttons: [
            {text: 'CANCELAR'},
            {text: 'EMPRESTAR', handler: data => this.emprestarEquipamento(item, data)}
        ]
    });

    form.present();
  }

  emprestarEquipamento(item, emprestimo) {

    let emp: boolean = new Equipamento(null,null,null,null,null,null).atualizarQuantidadeDisponiveisEquipamento(item.id, emprestimo.quantidade, false);
    if(emp) {
      new Emprestimo(item.id, emprestimo.solicitante, emprestimo.quantidade).adicionarEmprestimo();
      window.location.href = '/';
    }

  }

}
