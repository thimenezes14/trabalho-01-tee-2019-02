import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EquipamentoDetalhePage } from './equipamento-detalhe.page';

const routes: Routes = [
  {
    path: '',
    component: EquipamentoDetalhePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EquipamentoDetalhePage]
})
export class EquipamentoDetalhePageModule {}
