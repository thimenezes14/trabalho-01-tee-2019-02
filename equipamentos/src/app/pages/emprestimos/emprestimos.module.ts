import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { EquipamentosModule } from 'src/app/shared/components/equipamentos/equipamentos.module';

import {ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmprestimosPage } from './emprestimos.page';

const routes: Routes = [
  {
    path: '',
    component: EmprestimosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EquipamentosModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EmprestimosPage]
})
export class EmprestimosPageModule {}
