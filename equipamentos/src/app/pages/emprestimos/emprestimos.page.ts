import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Equipamento } from '../equipamentos/Equipamento';
import { Emprestimo } from './Emprestimo';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-emprestimos',
  templateUrl: './emprestimos.page.html',
  styleUrls: ['./emprestimos.page.scss'],
})
export class EmprestimosPage implements OnInit {

  emprestimoForm: FormGroup;
  solicitante: String;

  listaEquipamentos: Array<Equipamento>;
  listaEmprestimos: Array<Emprestimo>;

  constructor(private alert: AlertController) {
    this.listaEquipamentos = new Equipamento(null,null,null,null,null,null).mostrarEquipamentos();
    this.listaEmprestimos = new Emprestimo(null,null,null).mostrarEmprestimos();
  }

  ngOnInit() {
  }

  async excluir(equip, emp) {
    const form = await this.alert.create({
        header: 'CONFIRMAR EXCLUSÃO',
        message: 'Tem certeza que deseja excluir este empréstimo?',
        buttons: [
            {text: 'NÃO'},
            {text: 'SIM', handler: () => this.removerEmprestimo(equip, emp)}
        ]
    });

    form.present();
  
  }

  async devolver(equip, emp) {
    const form = await this.alert.create({
        header: 'CONFIRMAR DEVOLUÇÃO',
        message: 'Tem certeza que deseja devolver este empréstimo?',
        buttons: [
            {text: 'NÃO'},
            {text: 'SIM', handler: () => this.devolverEmprestimo(equip, emp)}
        ]
    });

    form.present();
  
  }

  atualizarListas() {
    this.listaEquipamentos = new Equipamento(null,null,null,null,null,null).mostrarEquipamentos();
    this.listaEmprestimos = new Emprestimo(null,null,null).mostrarEmprestimos();
  }

  removerEmprestimo(idEquipamento, idEmprestimo) {
    let quantidadeDevolvida = new Emprestimo(null, null, null).removerEmprestimo(idEmprestimo);
    new Equipamento(null,null,null,null,null,null).atualizarQuantidadeDisponiveisEquipamento(idEquipamento, quantidadeDevolvida, true);
    
    this.atualizarListas();
  }

  devolverEmprestimo(idEquipamento, idEmprestimo) {
    let quantidadeDevolvida = new Emprestimo(null, null, null).devolverEmprestimo(idEmprestimo);
    new Equipamento(null,null,null,null,null,null).atualizarQuantidadeDisponiveisEquipamento(idEquipamento, quantidadeDevolvida, true);
    
    this.atualizarListas();
  }

}
