export class Emprestimo {
    private id: Number;
    private idEquipamento: Number;
    private solicitante: string;
    private quantidade: Number;
    private dataHora: string;
    private localStorageName: string;

    constructor(idEquipamento: Number, solicitante: string, quantidade: Number) {
        this.id = new Date().getTime();
        this.idEquipamento = idEquipamento;
        this.solicitante = solicitante;
        this.quantidade = quantidade;
        this.dataHora = this.getDataHora();
        this.localStorageName = 'emprestimos';
    }

    public adicionarEmprestimo() {
        const emprestimo = {
            id: this.id,
            idEquipamento: this.idEquipamento,
            quantidade: this.quantidade,
            solicitante: this.solicitante,
            dataHora: this.dataHora
        }

        let listaEmprestimos = this.__agrupar(null);
        listaEmprestimos.push(emprestimo);

        let emprestimos = JSON.stringify(listaEmprestimos);
        localStorage.setItem(this.localStorageName, emprestimos);
    }

    public removerEmprestimo(idEmprestimo: Number) {
        let emprestimos = this.__agrupar(null);

        let emprestimoARemover = emprestimos.filter(emp => emp.id == idEmprestimo);
        let listaEmprestimos = emprestimos.filter(emp => emp.id != idEmprestimo);

        localStorage.setItem(this.localStorageName, JSON.stringify(listaEmprestimos));

        return emprestimoARemover[0].quantidade;
    }

    public detalharEmprestimo(idEmprestimo: Number) {
        let emprestimos = this.__agrupar(null);
        let emprestimoDetalhe = emprestimos.filter(emp => emp.id == idEmprestimo);

        return emprestimoDetalhe;
    }

    public devolverEmprestimo(idEmprestimo: any) {
        const devolucao = {
            id: new Date().getTime(),
            idEmprestimo,
            dataHora: this.getDataHora()
        }

        const devolucaoLocalStorage = 'devolucoes';
        let listaDevolucoes = this.__agrupar(devolucaoLocalStorage);
        listaDevolucoes.push(devolucao);

        let devolucoes = JSON.stringify(listaDevolucoes);
        localStorage.setItem(devolucaoLocalStorage, devolucoes);

        return this.removerEmprestimo(idEmprestimo);
    }

    public mostrarEmprestimos() {
        return (JSON.parse(localStorage.getItem(this.localStorageName)));
    }

    private getDataHora() {
        let ano = new Date().getFullYear();
        let mes = new Date().getMonth() + 1;
        let dia = new Date().getDate();

        let diaMesAno = (dia < 10 ? '0' : '') + dia + '-' + 
                        (mes < 10 ? '0' : '') + mes + '-' +
                        ano;

        let hora = new Date().getHours();
        let minuto = new Date().getMinutes();
        let segundo = new Date().getSeconds();

        let horaMinutoSegundo = (hora < 10 ? '0' : '') + hora + ':' +
                                (minuto < 10 ? '0' : '') + minuto + ':' +
                                (segundo < 10 ? '0' : '') + segundo;

        return diaMesAno + ' ' + horaMinutoSegundo;
    }

    private __agrupar(localStorageToChange: any) {
        const localStorageName = localStorageToChange == null ? this.localStorageName : localStorageToChange;
        
        let emprestimos = JSON.parse(localStorage.getItem(localStorageName));
        let listaEmprestimos = Array();
    
        if(emprestimos !== null && emprestimos.length > 0) {
          emprestimos.forEach(item => {
            listaEmprestimos.push(item);
          });
        }

        return listaEmprestimos;
    }
}